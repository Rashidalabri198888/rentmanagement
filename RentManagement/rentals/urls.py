from django.urls import path
from . import views

urlpatterns = [
    path('contracts/', views.ContractListView.as_view(), name='contrac_tlist'), 
    path('contracts/new/', views.ContractCreateView.as_view(), name='contract_new'),
    path('contracts/<int:pk>/', views.ContractDetailView.as_view(), name='contract_detail'),
    path('contrants/<int:pk>/edit/', views.ContractUpdateView.as_view(), name='contract_update'),
    path('contracts/', views.ContractDeleteView.as_view(), name='contract_delete'),
    path('payments/', views.PaymentListView.as_view(), name='payment_list'), 
    path('payments/new/', views.PaymentCreateView.as_view(), name='payment_new'), 
    path('letters/', views.LetterListView.as_view(), name='leeter_list'), 
    path('leeters/new/', views.LetterCreateView.as_view(), name='leeter_new')
]