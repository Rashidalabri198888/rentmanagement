from django import forms
from .models import Contract, Payment, Letter

class ContractForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = '__all__'
        
class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = '__all__'
        
class LetterForm(forms.ModelForm):
    class Meta:
        model = Letter
        fields = '__all__'