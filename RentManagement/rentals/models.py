from django.db import models

# Create your models here.
RENT_STATUS_CHOICES = [
    ('paid', 'مدفوع'),
    ('unpaid', 'غير مدفوع'),
]
class Contract(models.Model):
    tenant_name = models.CharField(max_length=100)
    property_address = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    rent_amount = models.DecimalField(max_digits=10, decimal_places=2)
    status = models.CharField(max_length=10,choices=RENT_STATUS_CHOICES, default='unpaid')
    
    def __str__(self):
        return self.tenant_name
        
class Payment(models.Model):
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE)
    payment_date = models.DateField()
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    
    def __str__(self):
        return f"{self.contract.tenant_name} - {self.payment_date}"
        
class Letter(models.Model):
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE)
    subject = models.CharField(max_length=200)
    content = models.TextField()
    date_send = models.DateField()
    
    def __str__(self):
        return self.subject