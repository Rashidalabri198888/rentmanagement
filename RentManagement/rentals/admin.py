from django.contrib import admin
from .models import Contract, Payment, Letter
# Register your models here.
class ContractAdmin(admin.ModelAdmin):
    list_display = ('tenant_name', 'property_address', 'start_date', 'end_date', 'rent_amount', 'status')
    search_fields = ('tenant_name', 'property_address')

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('contract', 'payment_date', 'amount')
    search_fields = ('contract__tenant_name',)

class LetterAdmin(admin.ModelAdmin):
    list_display = ('contract', 'subject', 'date_sent')
    search_fields = ('contract__tenant_name', 'subject')

admin.site.register(Contract, ContractAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Letter, LetterAdmin)