from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Contract, Payment, Letter
from .forms import ContractForm, PaymentForm, LetterForm
# Create your views here.
class ContractListView(ListView):
    model = Contract
    
class ContractDetailView(DetailView):
    model = Contract

class ContractCreateView(CreateView):
    model = Contract
    form_class = ContractForm
    success_url = reverse_lazy ('contract_list')
    
class ContractUpdateView(UpdateView):
    model = Contract
    form_class = ContractForm
    success_url = reverse_lazy('contract_list')

class ContractDeleteView(DeleteView):
    model = Contract
    success_url = reverse_lazy('contract_list')

class PaymentListView(ListView):
    model = Payment
    
class PaymentCreateView(CreateView):
    model = Payment
    form_class = PaymentForm
    success_url = reverse_lazy('payment_list')
    
class LetterListView(ListView):
    model = Letter
    
class LetterCreateView(CreateView):
    model = Letter
    form_class = LetterForm
    success_url = reverse_lazy('letter_list')